// Task 1

class Shape {
    func calculateArea() -> Double {
        fatalError("not implemented")
    }
    func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
}

class Rectangle : Shape {
    private let height, width : Double
    
    init(_ height : Double, _ width : Double) {
        self.height = height
        self.width = width
    }
    
    override func calculateArea() -> Double {
        return self.height * self.width
    }
    
    override func calculatePerimeter() -> Double {
        return self.height * 2 + self.width * 2
    }
}

class Circle : Shape {
    private let side : Double
    
    init(_ side : Double) {
        self.side = side
    }
    
    override func calculateArea() -> Double {
        return self.side * self.side
    }
    
    override func calculatePerimeter() -> Double {
        return self.side * 4
    }
}

class Square : Shape {
    private let radius : Double
    
    init(_ radius : Double) {
        self.radius = radius
    }
    
    override func calculateArea() -> Double {
        return self.radius * self.radius * Double.pi
    }
    
    override func calculatePerimeter() -> Double {
        return self.radius * Double.pi * 2
    }
}

let circle = Circle(5.5)
let square = Square(5.5)
let rectangle = Rectangle(4.4, 5.5)

let shapes : Array<Shape> = [circle, square, rectangle]

for shape in shapes {
    print("Area is \(shape.calculateArea())")
    print("Perimeter is \(shape.calculatePerimeter())")
}



// Task 2

func findIndex(ofString valueToFind: String, in array: [String]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}


func findIndexWithGenerics<T: Equatable> (ofValue valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}


let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
if let foundIndex = findIndexWithGenerics(ofValue: "лама", in: arrayOfString) {
    print("Индекс ламы: \(foundIndex)")
}

let arrayOfDouble = [22.88, 31.23, 0.009, 63.00013]
if let foundIndex = findIndexWithGenerics(ofValue: 0.009, in: arrayOfDouble) {
    print("Индекс числа 0.009: \(foundIndex)")
}

let arrayOfInt = [1, 2, 3, 4, 5]
if let foundIndex = findIndexWithGenerics(ofValue: 4, in: arrayOfInt) {
    print("Индекс числа 4: \(foundIndex)")
}


