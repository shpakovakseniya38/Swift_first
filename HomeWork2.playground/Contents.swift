import Foundation

// Task 1
var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

// Task 2
var milkPrice: Double = 3

// Task 3
milkPrice = 4.20

// Task 4
var milkBottleCount : Int? = nil
var profit : Double = 0.0
// Принудительное развертывание приведет к ошибке, если в паременной окажется nil,
// как сейчас здесь milkBottleCount.
if let count = milkBottleCount{
    profit = milkPrice * Double(count)
    print("Прибыль от продажи молока: \(profit)")
} else {
    print ("Иван приехал в магазин с пустым кузовом.")
}

// Task 5
var employeeList = [String]()
employeeList += ["Иван", "Марфа", "Андрей", "Петр", "Геннадий"]

// Task 6
var isEveryoneWorkHard : Bool = false
var workingHours = 39

if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}

print(isEveryoneWorkHard)

