class NameList {
    var names: Array<String> = []
    
    func addName(name: String){
        names.append(name)
    }
    
    func printNames() {
        names.sort()
        var currentLetter: Character = " "
        for name in names {
            let firstLetter = name.first ?? " "
            if firstLetter != currentLetter {
                currentLetter = firstLetter
                print(currentLetter)
            }
            print(name)
        }
    }
}

let list = NameList()
list.addName(name: "Акулова")
list.addName(name: "Валерьянов")
list.addName(name: "Бочкин")
list.addName(name: "Бабочкина")
list.addName(name: "Арбузов")
list.addName(name: "Васильева")
list.printNames()
