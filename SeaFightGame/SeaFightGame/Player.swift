import Foundation

final class Player {
    public var name: String
    public var gameBoard: Board
    public var ships: [[Int]]
    
    init(_ name: String) {
        self.name = name
        self.gameBoard = Board()
        self.ships = [[]]
        fillGameBoard()
    }
    
    private func fillOneField() {
        var isFilled = false
        while isFilled == false {
            gameBoard.printMap()
            let column = gameBoard.getColumnCoordinate()
            let line = gameBoard.getLineCoordinate()
            if gameBoard.checkShipPlace(column, line) {
                gameBoard.map[line - 1][column] = 1
                if ships[0].isEmpty {
                    ships[0] = [line - 1, column]
                } else {
                    ships.append([line - 1, column])
                }
                print("\n Теперь твоя карта выглядит так: \n")
                gameBoard.printMap()
                isFilled = true
            } else {
                print("\n ~~~~ Введенная координата уже занята. ~~~~\n")
            }
        }
    }
    
    public func fillGameBoard() {
        print("\n ~~~~ Привет, \(self.name)! ~~~~\n У тебя есть корабли: \n - однопалубный - 2 шт.\n - двухпалубный - 1 шт.\n")
        for i in 1...2 {
            print("\n ~~~~ Размести на карте \(i)-й однопалубный корабль. ~~~~\n")
            fillOneField()
        }
        print("\n ~~~~ Размести на карте единственный двухпалубный корабль. ~~~~\n")
        for _ in 1...2 {
            fillOneField()
        }
    }
    
    public func makeMove() -> [Int]{
        print("\n\n ~~~~ \(self.name), твой ход. ~~~~\n")
        return [gameBoard.getColumnCoordinate(), gameBoard.getLineCoordinate()]
    }
}

