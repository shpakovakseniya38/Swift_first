import Foundation

final class SeaFightGame {
    
    var player1: Player
    var player2: Player
    
    init() {
        print(" ~~~~ Введи имя первого игрока: ~~~~\n")
        let name1 = readLine() ?? ""
        if !name1.isEmpty {
            player1 = Player(name1)
        } else {
            player1 = Player("Игрок 1")
        }
        
        print("\n\n ~~~~ Введи имя второго игрока: ~~~~ \n")
        let name2 = readLine() ?? ""
        if !name2.isEmpty {
            player2 = Player(name2)
        } else {
            player2 = Player("Игрок 2")
        }
    }
    
    public func move(_ player: Player,_ coordinate: [Int]) -> Bool {
        var hitTarget = false
        let column = coordinate[0]
        let line = coordinate[1]
        if !player.gameBoard.checkShipPlace(column, line) {
            player.gameBoard.map[line - 1][column] = 0
            for (index, ship) in player.ships.enumerated() {
                if ship == [line - 1, column] {
                    player.ships.remove(at: index)
                }
            }
            hitTarget = true
        }
        return hitTarget
    }
    
    public func start() {
        var currentPlayer = player1
        var otherPlayer = player2
        var currentPlayerNumber = 1
        
        while !otherPlayer.ships.isEmpty {
            var moveResult: Bool
            
            repeat {
                moveResult = move(otherPlayer, currentPlayer.makeMove())
                if moveResult {
                    print("\n ~~~~ Попадание! ~~~~\n")
                    if otherPlayer.ships.isEmpty {
                        print("\n\n ~~~~ УРА!!! ПОБЕДИТЕЛЬ: \(currentPlayer.name) !!! ~~~~ \n\n")
                        return
                    }
                } else {
                    print("\n ~~~~ Промах! ~~~~\n")
                }
            } while moveResult == true
            
            if currentPlayerNumber == 1 {
                currentPlayer = player2
                otherPlayer = player1
                currentPlayerNumber = 2
            } else {
                currentPlayer = player1
                otherPlayer = player2
                currentPlayerNumber = 1
            }
        }
    }
}

let game = SeaFightGame()
game.start()
