import Foundation

final class Board {
    var map = Array(repeating: Array(repeating: 0, count: 4), count: 4)
    
    public func printMap(){
        var string = "  A B C D\n"
        for (lineIndex, line) in map.enumerated() {
            string += "\(lineIndex + 1) "
            for (index, field) in line.enumerated() {
                if field == 0 {
                    string += "- "
                } else {
                    string += "+ "
                }
                if index == 3 {
                    string += "\n"
                }
            }
        }
        print(string)
    }
    
    
    public func getColumnCoordinate() -> Int {
        var column: Int = -1
        while column < 0 {
            print("\nВведи координату столбца - букву A, B, C или D. \n")
            let input = readLine() ?? ""
            if !input.isEmpty {
                switch input {
                case "A": column = 0
                case "B": column = 1
                case "C": column = 2
                case "D": column = 3
                default:
                    print("\nНеправильная координата.\n")
                    column = -1
                }
            } else {
                print("\nНеправильная координата.\n")
                column = -1
            }
        }
        return column
    }
    
    public func getLineCoordinate() -> Int {
        var line: Int = -1
        while line < 0 {
            print("\nВведи координату строки - цифру 1, 2, 3 или 4. \n")
            let input = readLine() ?? ""
            if !input.isEmpty {
                line = Int(input) ?? -1
                if line > 4 {
                    print("\nНеправильная координата.\n")
                    line = -1
                }
            } else {
                print("\nНеправильная координата.\n")
                line = -1
            }
        }
        return line
    }
    
    public func checkShipPlace(_ column: Int, _ line: Int) -> Bool {
        if map[line - 1][column] == 0 {
            return true
        } else {
            return false
        }
    }
            
}
