import Foundation

// Task 1
func makeBuffer() -> (String) -> Void {
    var stringBuffer: String = ""
    
    func buffering (value: String) {
        if value.isEmpty {
            print(stringBuffer)
        } else {
            stringBuffer += value
        }
    }
    return buffering
}

var buffer = makeBuffer()

buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("")

// Task 2
func checkPrimeNumber(_ num: Int) -> Bool {
    if num <= 1 {
        return false
    } else {
        for i in 2 ..< num {
            if num % i == 0 {
                return false
            }
        }
        return true
    }
}

checkPrimeNumber(2)
checkPrimeNumber(8)
checkPrimeNumber(17)
