// Task 1

struct Candidate {
    enum Grade {
        case junior
        case middle
        case senior
    }
     
    let grade: Grade
    let requiredSalary: Int
    let fullName: String
}


protocol CandidateFilter {
    func filterCandidates(_ candidates: [Candidate]) -> [Candidate]
}


final class GradeFilter: CandidateFilter {
    private let grade: Candidate.Grade
    
    init(_ grade: Candidate.Grade) {
        self.grade = grade
    }
    
    func filterCandidates(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.grade == grade }
    }
}


final class SalaryFilter: CandidateFilter {
    private let salary: Int
    
    init(_ salary: Int) {
        self.salary = salary
    }
    
    func filterCandidates(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.requiredSalary <= salary }
    }
}


final class NameFilter: CandidateFilter {
    private let substring: String
    
    init(_ substring: String) {
        self.substring = substring
    }
    
    func filterCandidates(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.fullName.contains(substring) }
    }
}


func filteringCandidates (filterType: CandidateFilter, candidates: [Candidate]) {
    let filterResult = filterType.filterCandidates(candidates)
    for candidate in filterResult {
        print("Грейд: \(candidate.grade);  ФИО: \(candidate.fullName);  ЗП: \(candidate.requiredSalary)")
    }
    print("\n")
}

let candidates = [Candidate(grade: .junior, requiredSalary: 65000, fullName: "Петров Петр Петрович"),
                  Candidate(grade: .middle, requiredSalary: 95000, fullName: "Иванов Иван Иванович"),
                  Candidate(grade: .senior, requiredSalary: 125000, fullName: "Фортвенглер Виолетта Ардалионовна")]

filteringCandidates(filterType: GradeFilter(.middle), candidates: candidates)
filteringCandidates(filterType: SalaryFilter(90000), candidates: candidates)
filteringCandidates(filterType: NameFilter("ет"), candidates: candidates)


// Task 2

extension Candidate.Grade {
    var gradeNumber: Int {
        switch self {
        case .junior:
            return 1
        case .middle:
            return 2
        case .senior:
            return 3
        }
    }
}


final class GradeNumberFilter: CandidateFilter {
    private let grade: Candidate.Grade
    
    init(_ grade: Candidate.Grade) {
        self.grade = grade
    }
    
    func filterCandidates(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.grade.gradeNumber >= grade.gradeNumber }
    }
}

filteringCandidates(filterType: GradeNumberFilter(.middle), candidates: candidates)

